package pageobjectmodel;

import dev.failsafe.internal.util.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class HomePage {
   private WebDriver driver;
    String url = "https://automationexercise.com/";

    //Selectori
    By logoutButton = By.cssSelector("fa fa-lock");
    By productPage = By.xpath("//*[@class='material-icons card_travel']");




//Constructori
    public HomePage(WebDriver driver) {
        this.driver = driver;
        driver.get(url);
    }

    //actiuni


    public void clickProductButton(){
        driver.findElement(productPage).click();
    }



    public boolean isLogoutButtonDisplay(){
        return driver.findElement(logoutButton).isDisplayed();
    }
}
