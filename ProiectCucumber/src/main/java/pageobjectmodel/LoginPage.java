package pageobjectmodel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginPage{
   private  WebDriver driver;


    public String url = "https://automationexercise.com/login";

    // selectori
 public  By emailInput = By.name("email");
 public  By passwordInput = By.name("password");
 public  By loginButton= By.cssSelector("[class ='login-form'] [class ='btn btn-default']");



    //constructor


    public LoginPage(WebDriver driver) {

        this.driver = driver;
        this.driver.get(url);
    }

    // actiuni
    public void enterEmail(String email) {

        driver.findElement(emailInput).sendKeys(email);
    }

    public  void enterPassword(String password) {

        driver.findElement(passwordInput).sendKeys(password);
    }

    public  void clickLogin() {
      //   explicit wait
       driver.findElement(loginButton).click();
    }
    public void loginWithCredentials(String email, String password) {
        enterEmail(email);
        enterPassword(password);
        clickLogin();
    }
   }


