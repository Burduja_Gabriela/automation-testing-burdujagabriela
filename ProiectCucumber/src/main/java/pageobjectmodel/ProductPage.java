package pageobjectmodel;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage {

    private WebDriver driver;
    String url = "https://automationexercise.com/products";

    By firstProductOnThePage = By.xpath("//*[@src='/get_product_picture/1']");

    //Cpnstructori


    public ProductPage(WebDriver driver) {
        this.driver = driver;
        driver.get(url);
    }


}
