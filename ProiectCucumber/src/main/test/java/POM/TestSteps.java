package POM;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobjectmodel.LoginPage;
import utils.BaseTestClass2;

public class TestSteps extends BaseTestClass2 {

    String email = "username20072023@gmail.com";
    String password = "username20072023";


    //Open Login page
    @Test
    public void openLoginPage(){

        driver.get("https://automationexercise.com/login");
        System.out.println("The Login page is opened");
        Assert.assertEquals("https://automationexercise.com/login", driver.getCurrentUrl());

    }

    //Add username and password
    @Test
    public void addUsernameAndPassword(){
        driver.get("https://automationexercise.com/login");
        WebElement usernameInput = driver.findElement(By.name("email"));
        WebElement passwordInput = driver.findElement(By.name("password"));
        WebElement loginButton = driver.findElement(By.cssSelector("[class ='login-form'] [class ='btn btn-default']"));

        usernameInput.sendKeys(email);
        passwordInput.sendKeys(password);
        loginButton.click();
        Assert.assertEquals("https://automationexercise.com/", driver.getCurrentUrl());
        System.out.println("You are now logged into the page and can start shopping");
    }

    @Test
    public void loginWithPageObject(){

        LoginPage loginPage = new LoginPage(driver);

        loginPage.enterEmail(email);
        loginPage.enterPassword(password);
        loginPage.clickLogin();



    }

    @Test
    public void productButtonIsPresentInThePage() {

       driver.get("https://automationexercise.com/");
       WebElement productButton =  driver.findElement(By.xpath("//*[@href='/products']"));

        System.out.println("The Product button is display");

       Assert.assertEquals("\uE8F8 Products", productButton.getText())
       ;


    }

 //   @Test
 //   public void findBlueTopProduct(){
//        driver.get("https://automationexercise.com/products");
//        WebElement productBlueTop = driver.findElement(By.xpath("//*[@src='/get_product_picture/1']"));
 //       System.out.println("The Blue Top product is present in the Products page");
//        Assert.assertEquals("The fierst product img in the page is ", productBlueTop.getTagName());



 //   }

    @Test
    public void findBlueTopProduct(){
        driver.get("https://automationexercise.com/products");
        WebElement productBlueTop = driver.findElement(By.xpath("//*[@src='/get_product_picture/1']"));
        System.out.println("The Blue Top product is present in the Products page");




    }

    }




