package utils;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.time.Duration;

public class BaseTestClass2 {

protected static

WebDriver driver = new ChromeDriver();




    @Before
    public void initializeDriver() {
        System.out.println("Before superclass");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
    }

    @After
    public void quitDriver() {
        System.out.println("After superclass");
        driver.quit();
    }
}

