package runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

import static io.cucumber.junit.CucumberOptions.SnippetType.CAMELCASE;


    @RunWith(Cucumber.class)
    @CucumberOptions(
            dryRun = false,
            features = "src/test/java/features",
            glue = "stepdefinitions",
            tags = "@Negative",
            plugin = {"pretty", "html:target/NegativeTests.html"},
            snippets = CAMELCASE
    )
    public class NegativeTestRunner{

    }

