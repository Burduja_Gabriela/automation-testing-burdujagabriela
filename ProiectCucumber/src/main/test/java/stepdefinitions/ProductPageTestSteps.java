package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import pageobjectmodel.HomePage;
import pageobjectmodel.ProductPage;

import static stepdefinitions.Hooks.driver;

public class ProductPageTestSteps {
    HomePage homePage;
    ProductPage productPage;

    @Given("I am on home page")
    public void theHomePageIsOpened(){
        homePage = new HomePage(driver);
        System.out.println("The Home page is opened");

    }

    @When("The product button is present in the page")
    public void theProductButtonIsPresentInThePage(){
       WebElement productButton =  driver.findElement(By.xpath("//*[@href='/products']"));
        System.out.println("The Product button is display");
       Assert.assertEquals("\uE8F8 Products", productButton.getText());

    }

    @Then("If I click the Product button is open the product page")
    public void openProductPage(){
        System.out.println("Clicking the Products button the Product page is opened");
        homePage.clickProductButton();
    }

    @And("Find the first product img on the page")
    public void theFirstProductOnThePage(){
        productPage = new ProductPage(driver);
        WebElement firstProductImg = driver.findElement(By.xpath("//*[@src='/get_product_picture/1']"));
        System.out.println("The Blue Top product is present in the Products page");
        Assert.assertEquals("Fierst img is display", firstProductImg.getText());



    }



}
