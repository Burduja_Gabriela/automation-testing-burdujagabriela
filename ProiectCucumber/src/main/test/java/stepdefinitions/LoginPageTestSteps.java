package stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import org.junit.Assert;
import pageobjectmodel.LoginPage;

import static stepdefinitions.Hooks.driver;

public class LoginPageTestSteps {

    LoginPage loginPage;

 //  String email = "username1231112@gmail.com";
 //  String password = "username1231112";
    @Given("I am on register page")
    public void goToLoginPage(){
        System.out.println("Open Login page");
        loginPage = new LoginPage(driver);

    }

    @When("Fill the Email with {string} and password {string}")
    public void loginInThePage(String email, String password){

        System.out.println("Add Email Address and Password");

        loginPage.enterEmail(email);
        loginPage.enterPassword(password);
        System.out.println("Click the Login button");
        loginPage.clickLogin();

    }

    @And("Click the Login button")
    public void clickLoginButton(){
        System.out.println("Click on Login button");
        loginPage.clickLogin();

        Assert.assertEquals("https://automationexercise.com/", driver.getCurrentUrl());
        System.out.println("You are now logged into the page and can start shopping");
    }





}
